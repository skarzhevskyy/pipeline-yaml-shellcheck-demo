#!/bin/sh
# Check script embedded in GitLab CI YAML named *.yml
# https://candrews.integralblue.com/2022/02/shellcheck-scripts-embedded-in-gitlab-ci-yaml/

shellcheck_args="--color=always"
newline="$(printf '\nq')"
newline=${newline%q}

color_red='\033[0;31m'
color_off='\033[0m'

find . -type f -iname "*.yml" | while IFS= read -r file; do
  yq eval '.[] | select(tag=="!!map") | (.before_script,.script,.after_script) | select(. != null ) | path | ".[\"" + join("\"].[\"") + "\"]"' "${file}" | while IFS= read -r selector; do
    script=$(yq eval "${selector} | join(\"${newline}\")" "${file}")
      if ! printf '%s' "${script}" | shellcheck "${shellcheck_args}" -; then
        >&2 printf "\n${color_red}Error in %s in the script specified in %s:${color_off}\n%s\n" "${file}" "${selector}" "${script}"
        exit 1
      fi
  done
done
